<?php

class course_format_translator_sql_transformer
{
    private $inputData;

    private $mapTable = [
        'students' => 'mdl_user',
        'enrol' => 'mdl_user_enrolments',
    ];

    private $mapAction = [
        'SELECT COUNT' => ['how many', 'how much', 'calculate'],
        'SELECT' => ['get', 'select']
    ];

    private $mapFilter = [
        'students enrolled' => 'mdl_user_enrolments.userid = mdl_user.id',
        'enrolled today' => 'from_unixtime(mdl_user_enrolments.timecreated, \'%Y-%m-%d\') = CURDATE()',
        'enrolled yesterday' => 'from_unixtime(mdl_user_enrolments.timecreated, \'%Y-%m-%d\') = SUBDATE(CURDATE(), 1)',
        'enrolled tomorrow' => 'from_unixtime(mdl_user_enrolments.timecreated, \'%Y-%m-%d\') = DATE_ADD(CURDATE(), INTERVAL 1 DAY)',
    ];

    public function __construct($request)
    {
        $this->inputData = $request;
    }

    public function getSql() {
        $action = null;
        $tables = null;
        $filters = null;

        extract($this->parseRequest());

        if(!$action) throw new Exception('Action not specified');
        if(!$tables) throw new Exception('Table not specified');

        $sql = $this->buildSql($action, $tables, $filters);
        return $sql;

    }

    public function parseRequest()
    {
        $action = '';
        $tables = [];
        $filters = [];

        foreach ($this->mapAction as $sqlChunk => $currentActions) {
            foreach ($currentActions as $currentAction) {
                if ( stristr($this->inputData, $currentAction) ) {
                    $action = $sqlChunk;
                    break 2;
                }
            }
        }

        foreach ($this->mapTable as $key => $tableName) {
            if ( stristr($this->inputData, $key) ) {
                $tables[] = $tableName;
            }
        }

        foreach($this->mapFilter as $key => $filterName) {
            if ( stristr($this->inputData, $key )) {
                $filters[] = $filterName;
            }
        }


        return compact('action', 'tables', 'filters');
    }

    public function buildSql($action, $tables, $filters)
    {
        $sql = '';

        $sql .= $action;
        $mainTable = $tables[0];
        $mainTable = strtolower(substr($action, -5)) == 'count'? '(' . $mainTable . '.id) as count' : ' ' . $mainTable . '.*';

        $sql .= $mainTable;
        $sql .= ' FROM ' . join(', ', $tables);

        if($filters) {
            $sql .= ' WHERE ';
            $sql .= join(' AND ', $filters);
        }

        $sql .= ';';

        return $sql;
    }
}


