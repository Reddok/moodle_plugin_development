<?php

require_once('includes/init.php');

$PAGE->set_url(get_config('translator', 'search_url'), ['id' => $course->id]);
$PAGE->set_title('Native search');
$PAGE->set_heading($course->fullname);

echo $OUTPUT->header();

require_once('views/form.php');

require_once('includes/close.php');