<?php

require_once($_SERVER['DOCUMENT_ROOT'] . '/config.php');

require_once('../../lib.php');

$id        = required_param('id', PARAM_INT);
$params    = ['id' => $id];
$course    = $DB->get_record('course', $params, '*', MUST_EXIST);

$PAGE->set_cacheable(false);

context_helper::preload_course($course->id);
$context = context_course::instance($course->id, MUST_EXIST);

require_login($course);

require_once($CFG->dirroot.'/calendar/lib.php');

$PAGE->set_pagelayout('course');

$rendererPath = get_config('translator', 'renderer');
require_once($CFG->dirroot . '/course/format/translator/renderer.php');

$PAGE->get_renderer('format_translator');

