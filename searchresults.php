<?php

require_once('includes/init.php');
require_once('classes/sql_transformer.php');

$search     = trim(strip_tags(optional_param('search', '', PARAM_RAW)));
$translator = new course_format_translator_sql_transformer($search);


$PAGE->set_url(get_config('translator', 'search_url'), ['id' => $course->id]); // Defined here to avoid notices on errors etc

$PAGE->navbar->add('Search results', $PAGE->url);
$PAGE->set_title('Course search');
$PAGE->set_heading($course->fullname);

echo $OUTPUT->header();

$sql = $translator->getSql();
$results = $DB->get_records_sql($translator->getSql());

require('views/results.php');

require('includes/close.php');
